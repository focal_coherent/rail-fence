/* RailFence.java (teoyk, NYP)
	
	Sample Output:
	
	Testing RailFence.encrypt() and RailFence.decrypt()
	---------------------------------------------------
	Plain text (odd) : Hello
	Cipher text      : Hloel
	Decrypted text   : Hello
	
	Plain text (even): Hello!
	Cipher text      : Hloel!
	Decrypted text   : Hello!
 */

package practical2;

public class RailFence {

	public String encrypt(String plaintext)
	{
		byte[] plaintextBytes = plaintext.getBytes();
		String ciphertext = "";
		byte[] ciphertextBytes = new byte[plaintextBytes.length] ;
		int mid, counter = 0;
		
		if (plaintextBytes.length % 2 == 0) { // even position
			mid = plaintextBytes.length / 2 - 1;
		} else { // odd position
			mid = plaintextBytes.length / 2;
		}
		
		for (int i=0; i<plaintextBytes.length; i++) {
			if (i % 2 == 0){ // even position
				ciphertextBytes[i/mid] = plaintextBytes[i];
			} else { // odd position
				ciphertextBytes[i/mid + mid + 1] = plaintextBytes[i];
			}
		}
		ciphertext = new String(ciphertextBytes);
		
		return ciphertext;
	}

	public String decrypt(String ciphertext)
	{
		byte[] ciphertextBytes = ciphertext.getBytes();
		String plaintext = "";
		byte[] plaintextBytes = new byte[ciphertextBytes.length] ;
		int mid = 0;
		
		if( ciphertextBytes.length % 2 == 0) { // even position
			mid = ciphertextBytes.length / 2;
		} else { // odd position
			mid = ciphertextBytes.length / 2 + 1;
		}
		
		for (int i=0; i<ciphertextBytes.length; i++) {
			if( i < mid) { // arrange first half at even positions
				plaintextBytes[i*2] = ciphertextBytes[i];
			} else { // arrange second half at odd positions
				plaintextBytes[2*i-5] = ciphertextBytes[i];
			}
		}
		plaintext = new String(plaintextBytes);
		
		return plaintext;
	}

	public static void main(String[] args) {
		RailFence railFence = new RailFence();

		System.out.println("Testing RailFence.encrypt() and RailFence.decrypt()");
		System.out.println("---------------------------------------------------");
		
		System.out.println("Plain text (odd) : " + "Hello");
		System.out.println("Cipher text      : " + railFence.encrypt("Hello"));
		System.out.println("Decrypted text   : " + railFence.decrypt("Hloel"));
		System.out.println();

		System.out.println("Plain text (even): " + "Hello!");
		System.out.println("Cipher text      : " + railFence.encrypt("Hello!"));
		System.out.println("Decrypted text   : " + railFence.decrypt("Hloel!"));
	}
}
